angular.module('Kolour')
	.controller('HomeCtrl', function($scope, $window, $rootScope, $auth, $http) {

		$scope.isAuthenticated = function() {
			return $auth.isAuthenticated();
		};

		$scope.logout = function() {
			$auth.logout('http://localhost:8080/');
		}
	});