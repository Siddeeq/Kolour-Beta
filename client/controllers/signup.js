angular.module('Kolour')
	.controller('SignupCtrl', function($scope, $auth) {

		$scope.signup = function() {
			var user = {
				firstName: $scope.firstName,
				lastName: $scope.lastName,
				username: $scope.username,
				email: $scope.email,
				password: $scope.password
			};

			// Satellizer
			$auth.signup(user)
				.catch(function(response) {
					console.log(response.data);
				});
		};
	});