angular.module('Kolour', ['ngRoute', 'ngMessages', 'satellizer'])
	.config(function($routeProvider, $authProvider) {
		$routeProvider
			.when('/', {
	    		templateUrl: 'views/login.html',
	    		controller: 'LoginCtrl'
	  		})
	  		.when('/login', {
	    		templateUrl: 'views/login.html',
	    		controller: 'LoginCtrl'
	  		})
	  		// .when('/logout', {
	    	// 		templateUrl: 'views/logout.html',
	    	// 		controller: 'LogoutCtrl'
	  		// })
	  		.when('/signup', {
	    		templateUrl: 'views/signup.html',
	    		controller: 'SignupCtrl'
	  		})
	  		.when('/home', {
	    		templateUrl: 'views/home.html',
	    		controller: 'HomeCtrl'
	  		})
	  		.otherwise('/');

	  	$authProvider.loginRedirect = '/home';
	  	$authProvider.logoutRedirect = '/';
	  	$authProvider.signupRedirect = '/login';
	  	$authProvider.loginUrl = 'http://localhost:3000/auth/login';
	  	$authProvider.signupUrl = 'http://localhost:3000/auth/signup';
	})
	.run(function($rootScope, $window, $auth) {
    	if ($auth.isAuthenticated()) {
      		$rootScope.currentUser = JSON.parse($window.localStorage.currentUser);
    	}
  	});
  	