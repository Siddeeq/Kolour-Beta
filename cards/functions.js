// Ionic Starter App, v0.9.20

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ionic.contrib.ui.tinderCards'])


.config(function($stateProvider, $urlRouterProvider) {

})

.directive('noScroll', function($document) {

  return {
    restrict: 'A',
    link: function($scope, $element, $attr) {

      $document.on('touchmove', function(e) {
        e.preventDefault();
      });
    }
  }
})

.controller('CardsCtrl', function($scope, $http, $q, TDCardDelegate) {
  console.log('CARDS CTRL');

  // var getUsers = function() {
  //   // $http.get("users.json")
  //   // .then(function(response) {
  //   //   $scope.users = response.data;
  //   // })
  //   // .catch(function(response) {
  //   //   console.error('Users error', response.status, response.data);
  //   // })
  //   // .finally(function() {
  //   //   console.log("Finally finished getting users");
  //   // });
  //   var users = null;
  //   var defer = $q.defer();
  //   $http.get('users.json')
  //   .success(function(data) {
  //     defer.resolve(data);
  //   })
  //   .error(defer.reject);
  //   return defer.promise;
  // };

  // var cardTypes = getUsers();

  var cardTypes = [
    { image: 'https://pbs.twimg.com/profile_images/344513261577360027/47d9c92134daba9635fd92e639edbbac.jpeg' },
    { image: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png' },
    { image: 'https://pbs.twimg.com/profile_images/521660956158353409/d_ESrTWo.jpeg' },
    { image: 'https://pbs.twimg.com/profile_images/2727748211/c3d0981ae770f926eedf4eda7505b006.jpeg' },
    { image: 'http://pbs.twimg.com/profile_images/495773188173754368/06qhrEbb.jpeg' }
  ];

  $scope.cards = Array.prototype.slice.call(cardTypes, 0);

  $scope.cardDestroyed = function(index) {
    $scope.cards.splice(index, 1);
  };

  $scope.addCard = function() {
    var newCard = cardTypes[Math.floor(Math.random() * cardTypes.length)];
    newCard.id = Math.random();
    $scope.cards.push(angular.extend({}, newCard));
  };
})

.controller('CardCtrl', function($scope, TDCardDelegate) {
  $scope.cardSwipedLeft = function(index) {
    console.log('LEFT SWIPE');
    $scope.addCard();
  };
  $scope.cardSwipedRight = function(index) {
    console.log('RIGHT SWIPE');
    $scope.addCard();
  };
});