var path = require('path');

var async = require('async');
var bcrypt = require('bcryptjs');
var bodyParser = require('body-parser');
var colors = require('colors');
var cors = require('cors');
var express = require('express');
var logger = require('morgan');
var jwt = require('jwt-simple'); // used to create, sign and verify tokens 
var moment = require('moment');
var mongoose = require('mongoose');
var request = require('request');

var Feedly = require('feedly'); // wrapper for newsfeed api

var config = require('./config'); // get our config file 

/*
 |--------------------------------------------------------------------------
 | Generate JSON Web Token
 |--------------------------------------------------------------------------
*/
function createJWT(user) {
	var payload = {
		sub: user._id,
		iat: moment().unix(),
		exp: moment().add(14, 'days').unix()
	};
	return jwt.encode(payload, config.TOKEN_SECRET);
}

/*
 |--------------------------------------------------------------------------
 | Login Required Middleware
 |--------------------------------------------------------------------------
*/
function isAuthenticated(req, res, next) {
	if (!(req.headers && req.headers.authorization)) {
		return res.status(400).send({ message: 'You did not provide a JSON Web Token in the Authorization header.' });
	}

	var header = req.headers.authorization.split(' ');
	var token = header[1];
	var payload = null;

	try {
		payload = jwt.decode(token, config.TOKEN_SECRET);
	} catch (err) {
		return res.status(401).send({ message: err.message });
	}

	if (payload.exp <= moment().unix()) {
		return res.status(401).send({ message: 'Token has expired.' });
	}

	User.findById(payload.sub, function(err, user) {
		if (!user) {
			return res.status(400).send({ message: 'User no longer exists.' });
		}

		req.user = payload.sub;
		next();
	});

 // if (!req.headers.authorization) {
 //    	return res.status(401).send({ message: 'Please make sure your request has an Authorization header' });
 //  	}
 //  	var token = req.headers.authorization.split(' ')[1];

 //  	var payload = null;
 //  	try {
 //    	payload = jwt.decode(token, config.TOKEN_SECRET);
 //  	}
 //  	catch (err) {
 //    	return res.status(401).send({ message: err.message });
 //  	}

 //  	if (payload.exp <= moment().unix()) {
 //    	return res.status(401).send({ message: 'Token has expired' });
 //  	}
 //  	req.user = payload.sub;
 //  	next();
}

/*
 |--------------------------------------------------------------------------
 | Extract Username From Email
 |--------------------------------------------------------------------------
*/
function extractUsername(email) {
	var username = email.split("@")[0];
	return username;
}

// Get our mongoose model
var User = mongoose.model('User', new mongoose.Schema({
	firstName: String,
	lastName: String,
	username: String,
	email: { type: String, unique: true, lowercase: true },
	password: { type: String, select: false }
}));

mongoose.connect(config.MONGO_URI); // connect to database
mongoose.connection.on('error', function(err) {
	console.log("Error: Could not connect to MongoDB. Did you forget to run `mongod`?".red);
});

var app = express();
var router = express.Router();

app.set('port', process.env.PORT || 3000);
app.use(cors({ credentials: true, origin: true }));
app.use(logger('dev')); // use morgan to log requests to the console
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(express.static(path.join(__dirname, '../../client'))); // serve static files

/*
 |--------------------------------------------------------------------------
 | GET /api/
 |--------------------------------------------------------------------------
*/
router.get('/', function(req, res) {
	res.json({ message: 'Welcome to the Kolour Me API !' });
});

/*
 |--------------------------------------------------------------------------
 | GET /api/me | Route to return authenticated person
 |--------------------------------------------------------------------------
*/
router.get('/me', isAuthenticated, function(req, res) {
	User.findById(req.user, function(err, user) {
		res.json(user);
	});
});

/*
 |--------------------------------------------------------------------------
 | GET /api/users | Route to return all users
 |--------------------------------------------------------------------------
*/
router.get('/users', function(req, res) {
	User.find({}, function(err, users) {
		res.json(users);
	});
});

app.use('/api', router); // apply the routes to our application with the prefix /api

app.get('/protected', isAuthenticated, function(req, res) {
	console.log(req.user); // prints currently signed-in user object
});

/*
 |--------------------------------------------------------------------------
 | Login into Account
 |--------------------------------------------------------------------------
*/
app.post('/auth/login', function(req, res) {
  User.findOne({ email: req.body.email }, '+password', function(err, user) {
    if (!user) {
      return res.status(401).send({ message: { email: 'Incorrect email' } });
    }

    bcrypt.compare(req.body.password, user.password, function(err, isMatch) {
      if (!isMatch) {
        return res.status(401).send({ message: { password: 'Incorrect password' } });
      }

      user = user.toObject();
      delete user.password; // No need to send the password through server

      var token = createJWT(user);
      res.send({ token: token, user: user });
    });
  });
});

/*
 |--------------------------------------------------------------------------
 | Create Email and Password Account
 |--------------------------------------------------------------------------
*/
app.post('/auth/signup', function(req, res) {
	User.findOne({ email: req.body.email }, function(err, existingUser) {
		if (existingUser) {
			return res.status(409).send({ message: 'Email is already taken' });
		}

		var user = new User({
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			username: extractUsername(req.body.email),
			email: req.body.email,
			password: req.body.password
		});

		bcrypt.genSalt(10, function(err, salt) { 
			bcrypt.hash(user.password, salt, function(err, hash) { 
				user.password = hash; 

				user.save(function() {
					res.send({ token: createJWT(user), user: user });
				});
			});
		});
	});
});

app.get('/api/feed', function(req, res) {
	// var feed = new Feedly({
	// 	client_id: '7ed6ff9b-16d7-4b5a-b33b-11a8678fa610',
	// 	client_secret: 'AgAjG9h7ImEiOiJGZWVkbHkgRGV2ZWxvcGVyIiwiZSI6MTQ0OTU1Mzc2MjYxMywiaSI6IjdlZDZmZjliLTE2ZDctNGI1YS1iMzNiLTExYTg2NzhmYTYxMCIsInAiOjgsInQiOjEsInYiOiJwcm9kdWN0aW9uIiwidyI6IjIwMTUuMzciLCJ4Ijoic3RhbmRhcmQifQ:feedlydev',
	// 	port: 8080
	// });

	var feed = new Feedly({
		client_id: 'sandbox',
		client_secret: 'YNXZHOH3GPYO6DF7B43K',
		base: 'https://sandbox.feedly.com',
		port: 8080
	});

	feed.categories().then(function(results) {
		res.send({ results: results });
		console.log(results);
	}, 
	function(error) {
		res.send({ error: error });
		console.log(error);
	});
});

app.listen(app.get('port'), function() {
	console.log("Express server listening on port " + app.get('port'));
});